<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\Color\ColorController;
use App\Http\Controllers\Order\OrderController;
use App\Http\Controllers\User\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get(
    '/login', [AuthController::class,'index']
)->name('login');

Route::get('/', function () {
    return view('layouts.app');
})->middleware(['auth.shopify'])->name('home');

Route::group(['middleware' => ['auth.shopify']], function () {
    Route::resource('user', UsersController::class);
    Route::resource('order', OrderController::class);
    Route::get('order-list', [OrderController::class, 'orderList']);
    Route::post('reorder', [OrderController::class, 'reOrder']);
    Route::post('update-delivery-date', [OrderController::class, 'updateDeliveryDate']);
    Route::resource('color', ColorController::class);
});

Route::get('test', [UsersController::class, 'test']);

Route::get('flush', function (){
    request()->session()->flush();
});
