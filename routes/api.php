<?php

use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::group(['namespace' => 'Api'], function () {
    Route::post('login', [ApiController::class, 'login']);
    Route::get('get-orders', [ApiController::class, 'getOrders']);
    Route::post('fulfill-lineitem', [ApiController::class, 'fulfilledLineItem']);
    Route::post('lineitem-status', [ApiController::class, 'lineitemStatus']);
    Route::get('get-lineitem-total', [ApiController::class, 'lineItemTotal']);
    Route::get('test-notification', [ApiController::class, 'testNotification']);
});
