<?php

namespace App\Listeners;

use App\Events\CheckOrderUpdate;
use App\Models\LineItem;
use App\Models\Order;
use App\Models\OrderColor;
use App\Models\User;
use App\Traits\OrderController;
use App\Traits\PushNotificationTrait;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class OrderUpdate
{
    use PushNotificationTrait;
    use OrderController;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CheckOrderUpdate  $event
     * @return void
     */
    public function handle(CheckOrderUpdate $event) {
        logger('========== START :: LISTENER:: OrderUpdate ==========');
        try{
 
            $ids = $event->ids;
            $sh_orderId = $ids['sh_order_id'];
            $user = User::where('name', $ids['domain'] )->first();

            if( $user ){
                $sh_order_result = $user->api()->rest('GET', 'admin/orders/' . $sh_orderId);
               

                if( !$sh_order_result['errors'] ) {
                    $sh_order = $sh_order_result['body']['order'];

                    // if ($sh_order->source_name == 'web' || $sh_order->source_name == 'pos' || $sh_order->source_name == 'iphone' || $sh_order->source_name == 'shopify_draft_order'){ //                    $graphQlOrder = $this->getFulfillmentStatus($user->id, $sh_orderId, count( $sh_order->line_items ));

                        {
                            $is_exist_db_order = Order::where('order_id', $sh_orderId)->first();
                        }
                        $db_order = ($is_exist_db_order) ? $is_exist_db_order : new Order;
                        $db_order->order_id = $sh_orderId;
                        $db_order->name = $sh_order->name;
                        $db_order->email = $sh_order->email;
                        $db_order->currency = $sh_order->currency;
                        $db_order->price = $sh_order->total_price;
                        $db_order->financial_status = $sh_order->financial_status;
    //                    $db_order->fulfillment_status = (!empty($graphQlOrder)) ? strtolower($graphQlOrder['displayFulfillmentStatus']) : $sh_order->fulfillment_status;
                        $db_order->fulfillment_status = ($sh_order->fulfillment_status) ? $sh_order->fulfillment_status : 'unfulfilled';
                        $db_order->source_name = $sh_order->source_name;
                        $db_order->notes = $sh_order->note;

                        $note_attr = $sh_order->note_attributes;


                        if( !empty($note_attr) ){                        	

                            $note_attr_array = (array) $note_attr;
                            $note_attr_array = $note_attr_array['container'];
													
							
                            foreach ( $note_attr as $nkey=>$nval ){
                              
                                // START ::  for shipping
                                if($nval['name']=="Checkout-Method" && $nval['value']=="shipping"){

                                    logger("====for shipping====");

                                    $att_id = array_search('Shipping-Date', array_column($note_attr_array, 'name'));

                                    $attr_date = $note_attr[$att_id]['value'];

                                    $db_order->delivery_date = date('Y-m-d H:i:s', strtotime($attr_date));
                                    $db_order->delivery_type = $nval['value'];

                                    logger($db_order->delivery_date);

                                }
                                //END ::  for shipping


                                // START ::  For local delivery

                                if($nval['name']=="Checkout-Method" && $nval['value']=="delivery"){

                                    logger("====For local delivery===");

                                    $att_id = array_search('Delivery-Date', array_column($note_attr_array, 'name'));

                                    $attr_date = $note_attr[$att_id]['value'];

                                    $db_order->delivery_date = date('Y-m-d H:i:s', strtotime($attr_date));
                                    $db_order->delivery_type = $nval['value'];


                                    logger($db_order->delivery_date);

                                }

                                //END :: For local delivery


                                // START ::  for pickup

                                if($nval['name']=="Checkout-Method" && $nval['value']=="pickup"){

                                    logger("====For pickup===");

                                    $att_date_id = array_search('Pickup-Date', array_column($note_attr_array, 'name'));

                                    logger("att_date_id");
                                    logger($att_date_id);

                                    $attr_date = $note_attr[$att_date_id]['value'];

                                    $att_time_id = array_search('Pickup-Time', array_column($note_attr_array, 'name'));

                                    $attr_time = $note_attr[$att_time_id]['value'];

                                    $delivery_date = $attr_date." ".$attr_time;

                                    logger("before delivery_date ");
                                    logger($delivery_date);

                                    $db_order->delivery_date = date('Y-m-d H:i:s', strtotime($delivery_date));

                                    $db_order->delivery_type = $nval['value'];

									logger("after delivery_date ");
                                    logger($db_order->delivery_date);

                                }

                                //END :: for pickup

                            }
                        }

                        $db_order->order_created_at = date('Y-m-d H:i:s', strtotime($sh_order->created_at));
                        $db_order->cancelled_at = ($sh_order->cancelled_at) ? date('Y-m-d H:i:s',
                            strtotime($sh_order->cancelled_at)) : null;
                        $db_order->position = Order::count() + 1;

                        $db_order->order_tagged = ($is_exist_db_order) ? $is_exist_db_order->order_tagged : 'begin';
                        $db_order->save();

                        $lineItems = $sh_order->line_items;

                        $lineItemFulfillment = [];
                        if (!empty($graphQlOrder)) {
                            $glLinetems = $graphQlOrder['lineItems']['edges'];
                            if (is_array($glLinetems) && !empty($glLinetems)) {
                                foreach ($glLinetems as $glkey => $glval) {
                                    $gl = $glval['node'];
                                    $gllineitemid = str_replace("gid://shopify/LineItem/", "", $gl['id']);
                                    $lineItemFulfillment[$gllineitemid] = $gl['fulfillmentStatus'];
                                }
                            }
                        }

                        if (count($lineItems) > 0) {
                            foreach ($lineItems as $lkey => $lval) {
                                $is_exist_lineitem = LineItem::where('lineitem_id', $lval->id)->where('order_id',
                                    $sh_orderId)->where('db_order_id', $db_order->id)->first();

                                $sh_product = $this->getShopifyProduct($lval->product_id, $user);
                                $sh_variant = $this->getShopifyVariant($lval->variant_id, $user);

                                $db_lineitem = ($is_exist_lineitem) ? $is_exist_lineitem : new LineItem;
                                $db_lineitem->db_order_id = $db_order->id;
                                $db_lineitem->lineitem_id = $lval->id;
                                $db_lineitem->order_id = $sh_orderId;
                                $db_lineitem->product_id = $lval->product_id;
                                $db_lineitem->variant_id = $lval->variant_id;
                                $db_lineitem->location_id = (@$lval->origin_location->id) ? $lval->origin_location->id : '';
                                $db_lineitem->title = $lval->title;
                                $db_lineitem->image = $sh_product['image'];
                                $db_lineitem->qty = $lval->quantity;
                                $db_lineitem->sku = $lval->sku;
                                $db_lineitem->barcode = $sh_variant['barcode'];
                                $db_lineitem->price = $lval->price;
    //                            $db_lineitem->weight = $lval->price;
                                $db_lineitem->weight_unit = $lval->grams;
    //                            $db_lineitem->fulfillment_status = (@$lineItemFulfillment[$lval->id]) ? $lineItemFulfillment[$lval->id] : $lval->fulfillment_status;
                                $db_lineitem->fulfillment_status = ($lval->fulfillment_status) ? $lval->fulfillment_status : 'unfulfilled';
                                $db_lineitem->tag = str_replace(" ", "", $sh_product['tags']);
                                $db_lineitem->save();
                            }
                        }

                        if (!$is_exist_db_order) {
                            $users = $this->getTaggedUser($db_order->id);
                            $data = $this->getNotificationOrder($db_order->id);
                            $this->sendNotificationH($data, $users, 'New order received!');
                        }
                    // }else {
                    //     logger($sh_order->source_name);
                    // }
                }else{
                    logger(json_encode($sh_order_result));
                }
            }else{
                logger('========== ERROR :: LISTENER:: User not found... ==========');
                logger('DOMAIN :: ' . $ids['domain']);
            }

            logger('========== END :: LISTENER:: OrderUpdate ==========');
        }catch ( \Exception $e ){
            logger('========== ERROR :: LISTENER:: OrderUpdate ==========');
            logger($e);
        }
    }

    public function getShopifyProduct($sh_product_id, $user){
        try{
            logger('========== START :: getShopifyProduct ==========');

            $endPoint = 'admin/products/' . $sh_product_id . '.json';
            $parameter['fields'] = 'image,tags';
            $sh_product_result = $user->api()->rest('GET', $endPoint, $parameter);

            $product['image'] = '';
            $product['tags'] = '';

            if( !$sh_product_result['errors'] ){
                $sh_product = $sh_product_result['body']['product'];
                $product['image'] = ( @$sh_product['image']['src'] ) ? $sh_product['image']['src'] : '';
                $product['tags'] = $sh_product['tags'];
            }else{
                logger(json_encode($sh_product_result));
            }

            logger('========== END :: getShopifyProduct ==========');

            return $product;
        }catch( \Exception $e ){
            logger('========== ERROR :: getShopifyProduct ==========');
            logger($e);
        }
    }

    public function getShopifyVariant($sh_variant_id, $user){
        try{
            logger('========== START :: getShopifyVariant ==========');

            $endPoint = 'admin/variants/' . $sh_variant_id . '.json';
            $parameter['fields'] = 'barcode';
            $sh_variant_result = $user->api()->rest('GET', $endPoint, $parameter);

            $variant['barcode'] = '';

            if( !$sh_variant_result['errors'] ){
                $sh_variant = $sh_variant_result['body']['variant'];
                $variant['barcode'] = $sh_variant['barcode'];
            }else{
                logger(json_encode($sh_variant_result));
            }

            logger('========== END :: getShopifyVariant ==========');

            return $variant;
        }catch( \Exception $e ){
            logger('========== ERROR :: getShopifyVariant ==========');
            logger($e);
        }
    }
}
