<?php

namespace App\Traits;

use App\Models\LineItem;
use App\Models\Order;
use App\Models\OrderColor;
use App\Models\User;
use Osiset\BasicShopifyAPI\BasicShopifyAPI;
use Osiset\BasicShopifyAPI\Options;
use Osiset\BasicShopifyAPI\Session;

/**
 * Trait OrderController.
 */
trait OrderController
{
    public function getOrderByUser($userId, $filter){
        try{
            logger('========== START:: getOrderByUser =========');
            $user = User::find($userId);

            $data['isSuccess'] = true;
            if( $user ){
                $tags = explode(',', $user->tags);

                $db_order = Order::select('orders.id', 'orders.order_id','orders.name', 'orders.order_tagged', 'orders.currency', 'orders.price', 'orders.source_name', 'orders.is_urgent', 'orders.delivery_date', 'orders.notes', 'line_items.title', 'line_items.image', 'line_items.qty', 'line_items.sku', 'line_items.price', 'line_items.fulfillment_status','line_items.status', 'line_items.fulfilled_by', 'line_items.fulfilled_at')->join('line_items', 'line_items.db_order_id', '=', 'orders.id')
                    ->Where(function ($query) use($tags, $filter) {
                        for ($i = 0; $i < count($tags); $i++){
                            $query->orwhere('line_items.tag', 'like',  '%' . $tags[$i] .'%');
                            $query->whereIn('line_items.fulfillment_status', explode(',', $filter));
                        }
                    })->orderBy('orders.position', 'asc')->get()->toArray();

                $o = [];
                if( count($db_order) > 0){
                    $orderColor = OrderColor::get()->pluck('color', 'order_status')->toArray();
                    foreach ( $db_order as $key=>$val ){
                        $user = User::find($val['fulfilled_by']);
                        $val['fulfilled_by'] = ($user) ? $user->email : '';
                        if( @$o[$val['name']] ){
                        }else{
                            $o[$val['name']]['line_items'] = [];
//                            $o[$val['name']] = [];
                        }
//                        array_push($o[$val['name']], $val);
                        $o[$val['name']]['order_name'] = $val['name'];
                        $o[$val['name']]['is_urgent'] = (bool)$val['is_urgent'];
                        $o[$val['name']]['order_tagged'] = $orderColor[$val['order_tagged']];
                        $o[$val['name']]['delivery_date'] = $val['delivery_date'];
                        $o[$val['name']]['notes'] = $val['notes'];
                        $o[$val['name']]['id'] = $val['id'];
                        array_push($o[$val['name']]['line_items'], $val);
                    }
                }
                $data['data'] = array_values($o);
//                $data['data'] = $o;
            }else{
                $data['data'] = 'User not found...';
                $data['isSuccess'] = false;
            }
            return $data;
        }catch( \Exception $e ){
            logger('========== ERROR:: getOrderByUser =========');
            logger($e);
            dd($e);
        }
    }

    public function getApiOrderByUser($inputes, $action){
        try{
            logger('========== START:: getOrderByUser =========');
            $userId = $inputes['user_id'];
            $source_name = $inputes['source_name'];
            $fulfillment_status = $inputes['fulfillment_status'];

            $user = User::find($userId);

            $data['isSuccess'] = true;
            if( $user ){
                $tags = explode(',', $user->tags);

//                $db_order = Order::select('orders.id','orders.order_id','orders.name', 'orders.currency', 'orders.price', 'orders.source_name', 'orders.is_urgent','orders.order_tagged', 'line_items.lineitem_id', 'line_items.title', 'line_items.image','line_items.weight', 'line_items.qty','line_items.status', 'line_items.sku','line_items.barcode', 'line_items.price', 'line_items.fulfillment_status', 'line_items.fulfilled_by', 'line_items.fulfilled_at')->join('line_items', 'line_items.db_order_id', '=', 'orders.id')
//                    ->where('source_name', $source_name)
//                    ->Where(function ($query) use($tags) {
//                        for ($i = 0; $i < count($tags); $i++){
//                            $query->orwhere('line_items.tag', 'like',  '%' . $tags[$i] .'%');
//                        }
//                    })->where('orders.fulfillment_status', $fulfillment_status)->orderBy('orders.position', 'asc')->get();
                $db_order = $this->getApiQuery($inputes);
                $o = [];
                if( $action == 'order' ){
                    if( count($db_order) > 0){
                        $orderColor = OrderColor::get()->pluck('color', 'order_status')->toArray();
                        foreach ( $db_order as $key=>$val ){
                            if( @$o[$val->name] ){
                            }else{
                                $o[$val->name]['line_items'] = [];
                            }
                            $o[$val['name']]['order_name'] = $val['name'];
                            $o[$val['name']]['order_tagged'] = $orderColor[$val['order_tagged']];
                            $o[$val['name']]['source_name'] = $val['source_name'];
                            $val["delivery_date"] = ($val["delivery_date"]) ? $val["delivery_date"] : $val['created_at'];
                            $val["delivery_type"] = ($val["delivery_type"]) ? $val["delivery_type"] : 'delivery';
                            array_push($o[$val->name]['line_items'], $val);
                        }
                    }
                    $data['data'] = array_values($o);
                }else{
                    if( count($db_order) > 0){
                        foreach ( $db_order as $key=>$val ){
                            if( @$o[$val->title] ){
                                $o[$val->title]['total'] = $o[$val->title]['total'] + $val['qty'];
                            }else{
                                $o[$val->title] = [];
                                $o[$val->title]['title'] = $val['title'];
                                $o[$val->title]['weight'] = $val['weight'];
                                $o[$val->title]['total'] = $val['qty'];
                            }
                        }
                    }
                    $data['data'] = array_values($o);
                }
            }else{
                $data['data'] = 'User not found...';
                $data['isSuccess'] = false;
            }
            return $data;
        }catch( \Exception $e ){
            logger('========== ERROR:: getOrderByUser =========');
            dump($e);
            logger($e);
        }
    }

    public function getLineItemCountByUser($inputes, $action){
        try{
            logger('========== START:: getOrderByUser =========');
            $user = User::find($inputes['user_id']);

            $data['isSuccess'] = true;
            if( $user ){
                $tags = explode(',', $user->tags);

//                $db_order = Order::select('orders.id','orders.order_id','orders.name', 'orders.currency', 'orders.price', 'orders.fulfillment_status', 'orders.source_name', 'orders.is_urgent','orders.order_tagged', 'line_items.lineitem_id', 'line_items.title', 'line_items.image', 'line_items.qty', 'line_items.sku', 'line_items.barcode', 'line_items.price', 'line_items.fulfillment_status', 'line_items.fulfilled_by', 'line_items.fulfilled_at')->join('line_items', 'line_items.db_order_id', '=', 'orders.id')
//                    ->Where(function ($query) use($tags) {
//                        for ($i = 0; $i < count($tags); $i++){
//                            $query->orwhere('line_items.tag', 'like',  '%' . $tags[$i] .'%');
//                        }
//                    })->where('orders.fulfillment_status', '!=', 'fulfilled')->orderBy('orders.position', 'asc')->get();
                // $db_order = Order::select('orders.id','orders.order_id','orders.name', 'orders.is_urgent', 'orders.currency', 'orders.price', 'orders.fulfillment_status', 'orders.source_name', 'line_items.lineitem_id', 'line_items.title', 'line_items.image', 'line_items.qty', 'line_items.sku', 'line_items.price', 'line_items.fulfillment_status')->join('line_items', 'line_items.db_order_id', '=', 'orders.id')
                //     ->Where(function ($query) use($tags) {
                //         for ($i = 0; $i < count($tags); $i++){
                //             $query->where('line_items.tag', 'like',  '%' . $tags[$i] .'%');
                //             $query->where('line_items.fulfillment_status', '!=', 'fulfilled');
                //         }
                //     })->where('orders.fulfillment_status', '!=', 'fulfilled')->orderBy('orders.position', 'asc')->get();
                $db_order = $this->getApiQuery($inputes);
                if( $action == 'order' ){
                    $o = [];
                    if( count($db_order) > 0){
                        $orderColor = OrderColor::get()->pluck('color', 'order_status')->toArray();
                        foreach ( $db_order as $key=>$val ){
                            if( @$o[$val->name] ){
                            }else{
                                $o[$val->name]['line_items'] = [];
                            }
                            $o[$val['name']]['order_name'] = $val['name'];
                            $o[$val['name']]['order_tagged'] = $orderColor[$val['order_tagged']];
                            array_push($o[$val->name]['line_items'], $val);
                        }
                    }
                    $data['data'] = array_values($o);
                }else{

                }

            }else{
                $data['data'] = 'User not found...';
                $data['isSuccess'] = false;
            }
            return $data;
        }catch( \Exception $e ){
            logger('========== ERROR:: getOrderByUser =========');
            logger($e);
        }
    }

    public function getApiQuery($inputes){
        $userId = $inputes['user_id'];
        $source_name = $inputes['source_name'];
        $fulfillment_status = $inputes['fulfillment_status'];
        $delivery_date = (@$inputes['delivery_date']) ? $inputes['delivery_date'] : '';

        $user = User::find($userId);

        $tags = explode(',', $user->tags);

        $db_order = Order::select('orders.id','orders.order_id','orders.name', 'orders.currency', 'orders.price', 'orders.source_name', 'orders.is_urgent','orders.order_tagged','orders.delivery_date', 'orders.delivery_type','orders.order_created_at', 'orders.notes', 'line_items.lineitem_id', 'line_items.title', 'line_items.image','line_items.weight', 'line_items.qty','line_items.status', 'line_items.sku','line_items.barcode', 'line_items.price', 'line_items.fulfillment_status', 'line_items.fulfilled_by', 'line_items.fulfilled_at')->join('line_items', 'line_items.db_order_id', '=', 'orders.id')
            ->Where(function ($query) use($source_name) {
                if( $source_name == 'pos' ){
                    $query->where('source_name', $source_name);
                }else{
                    $query->where('source_name', '!=', 'pos');
                }
            })
            ->Where(function ($query) use($tags) {
                for ($i = 0; $i < count($tags); $i++){
                    $query->orwhere('line_items.tag', 'like',  '%' . $tags[$i] .'%');
                }
            })
            ->Where(function ($query) use($delivery_date) {
                if( $delivery_date != '' ){
                    $query->where('orders.delivery_date', $delivery_date);
                }
            })
            ->where('orders.fulfillment_status', $fulfillment_status)->orderBy('orders.delivery_date', 'desc')->get();

        return $db_order;
    }

    public function getNotificationOrder($db_order_id){
        try{
            logger('========== START:: getNotificationOrder =========');

            $db_order = Order::select('orders.id','orders.order_id','orders.name', 'orders.currency', 'orders.price', 'orders.fulfillment_status AS order_fulfillment_status', 'orders.source_name', 'orders.is_urgent','orders.order_tagged', 'line_items.lineitem_id', 'line_items.title', 'line_items.image','line_items.weight', 'line_items.qty', 'line_items.sku', 'line_items.price', 'line_items.fulfillment_status', 'line_items.fulfilled_by', 'line_items.fulfilled_at')->join('line_items', 'line_items.db_order_id', '=', 'orders.id')->where('orders.id', $db_order_id)->get()->toArray();

            $data = [];
            if( count($db_order) > 0 ){
                $orderColor = OrderColor::get()->pluck('color', 'order_status')->toArray();
                $data['order_name'] = $db_order[0]['name'];
                $data['order_tagged'] = $orderColor[$db_order[0]['order_tagged']];
                $data['source_name'] = $db_order[0]['source_name'];
                $data['fulfillment_status'] = $db_order[0]['order_fulfillment_status'];
                $data['line_items'] = $db_order;
            }

            return $data;
        }catch( \Exception $e ){
            logger('========== ERROR:: getNotificationOrder =========');
            logger($e);
        }
    }

    public function getTaggedUser($db_order_id){
        try{
            logger('========== START:: getTaggedUser =========');
            $line_items = LineItem::where('db_order_id', $db_order_id)->get()->pluck('tag');
            $tags = [];
            foreach ( $line_items as $lkey=>$lval ){
                $ltag = explode(',', $lval);
                $tags = array_merge($tags, $ltag);
            }
            $user = User::Where(function ($query) use ($tags) {
                for ($i = 0; $i < count($tags); $i++) {
                    $query->orwhere('tags', 'like', '%' . $tags[$i] . '%');
                }
            })->get()->pluck('id');
            return $user;
        }catch( \Exception $e ){
            logger('========== ERROR:: getTaggedUser =========');
            logger($e);
        }
    }

    public function getFulfillmentStatus($shop_id, $orderId, $lineitemCount){
        try{
            logger('========== START:: getFulfillmentStatus =========');
            $query = '
                {
                  order(id: "gid://shopify/Order/'. $orderId .'") {
                    displayFulfillmentStatus,
                    location
                    lineItems(first: '. $lineitemCount .') {
                      edges {
                        node {
                          id
                          fulfillmentStatus
                        }
                      }
                    }
                  }
                }
            ';
            $result = $this->graphQLRequest($shop_id, $query, [], $lineitemCount);
            if( !$result['errors'] )
                return (@$result['body']->container['data']['order']) ? $result['body']->container['data']['order'] : [];
            else
                return [];
        }catch ( \Exception $e ){
            logger('========== ERROR:: getFulfillmentStatus =========');
            logger($e);
        }
    }

    public function graphQLRequest($shop_id, $query, $parameter = []){
        try{
            logger("====================== QUERY ======================");
            logger($query);
            $shop = User::find($shop_id);
            $options = new Options();
            $options->setVersion(env('SHOPIFY_API_VERSION'));
            $api = new BasicShopifyAPI($options);
            $api->setSession(new Session(
                $shop->name, $shop->password));
            return $api->graph($query, $parameter);
        }catch( \Exception $e )
        {
            logger('============ ERROR:: graphQLRequest ===========');
            logger(json_encode($e));
        }
    }

    public function fulfilledOrder($user_id, $orderId, $lineItemId){
        try{
            logger('========== START:: fulfilledOrder =========');
            $shop = User::where('name', '!=', null)->first();
            $db_lineItem = LineItem::where('order_id', $orderId)->where('lineitem_id', $lineItemId)->first();
            $location_id = $this->getLocationId(1, $db_lineItem->variant_id);
            $lineitem = [
                'fulfillment' => [
                    'location_id' => $location_id,
                    'tracking_number' => null,
                    'line_items' => [
                        [
                            'id'=> $lineItemId
                        ]
                    ]
                ]
            ];

            $endPoint = '/admin/orders/' . $orderId . '/fulfillments.json';
            $result = $shop->api()->rest('POST', $endPoint, $lineitem);

            if( !$result['errors'] ){
                $db_lineItem->fulfillment_status = 'fulfilled';
                $db_lineItem->status = 'complete';
                $db_lineItem->fulfilled_by = $user_id;
                $db_lineItem->fulfilled_at = date('Y-m-d H:i:s');
                $db_lineItem->save();

                $db_unfulfilled_count = LineItem::where('order_id', $orderId)->where('fulfillment_status', '!=', 'fulfilled')->count();
                $db_order = Order::where('order_id', $orderId)->first();
                if( $db_order && ($db_unfulfilled_count == 0)){
                    $db_order->fulfillment_status = 'fulfilled';
                    $db_order->order_tagged = 'complete';
                    $db_order->save();
                }
                $msg = 'LineItem fulfilled successfully...';
            }else{
                $msg = $result['body'];
            }
            return $msg;
        }catch ( \Exception $e ){
            logger('========== ERROR:: fulfilledOrder =========');
            logger($e);
        }
    }

    public function getLocationId($shop_id, $variant_id){
        try{
            logger('========== START:: getLocationId =========');
            $query = '
                {
                  productVariant(id: "gid://shopify/ProductVariant/'. $variant_id .'") {
                    id
                    inventoryItem {
                      id
                      inventoryLevels(first: 10) {
                        edges {
                          node {
                            id
                            available
                            location {
                              id
                            }
                          }
                        }
                      }
                    }
                  }
                }
            ';
            $result = $this->graphQLRequest($shop_id, $query, []);
            if( !$result['errors'] ){
                $inventory = (@$result['body']->container['data']['productVariant']['inventoryItem']['inventoryLevels']['edges']) ? $result['body']->container['data']['productVariant']['inventoryItem']['inventoryLevels']['edges'] : [];

                if( !empty($inventory) ){
                    $location = $inventory[0]['node']['location'];
                    return (@$location['id']) ? str_replace('gid://shopify/Location/', '', $location['id']) : '';
                }else{
                    return '';
                }
            }else{
               logger(json_encode($result));
               return '';
            }
        }catch( \Exception $e ){
            logger('========== START:: getLocationId =========');
            logger($e);
        }
    }
}
