<?php

namespace App\Jobs;

use App\Models\OrderColor;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AfterAuthenticateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            logger('=============== START:: AfterAuthenticationJob =============');

            $orderColors = OrderColor::count();
            if( $orderColors == 0 ){
                $status = ['begin' => '#ffffff', 'in_progress' => '#ffffff', 'complete' => '#ffffff', 'urgent' => '#ffffff'];
                foreach ( $status as $key=>$val ){
                    $orderColor = new OrderColor;
                    $orderColor->order_status = $key;
                    $orderColor->color = $val;
                    $orderColor->save();
                }
            }
        }catch( \Exception $e ){
            logger('=============== ERROR:: AfterAuthenticationJob =============');
            logger($e);
        }
    }
}
