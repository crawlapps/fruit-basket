<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    public function hasManyLineItem(){
        return $this->hasMany(LineItem::class, 'lineitem_id', 'id' );
    }
}
