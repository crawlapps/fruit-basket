<?php

use Symfony\Component\Intl\Currencies;

if (!function_exists('currencyH')) {

    /**
     * @return mixed
     * @return mixed
     */
    function currencyH($c)
    {
        return Currencies::getSymbol($c);
    }
}

if (!function_exists('sendNotificationH')) {
    /**
     * @param $str
     * @return bool|false|string
     */
    function sendNotificationH($data, $users, $message)
    {
        $rest_api_key = env('ONESIGNAL_REST_API_KEY');
        $app_id = env('ONESIGNAL_APP_ID');

        $content = array(
            "en" => $message,
        );

        foreach ( $users as $key=>$val ){
            $filters[] = (array("field" => "tag", "key" => "user_id", "relation" => "=", "value" => $val));

            if( count($users) != ($key + 1) ){
                $filters[] = ["operator"=> "OR"];
            }
        }
        $fields = array(
            'filters' => $filters,
            'app_id' => $app_id,
            'data' => $data,
            'contents' => $content,
        );

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic '. $rest_api_key ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        \Log::info($response);
//        dump($response);
        return $response;

        \Log::info($response);
    }
}
