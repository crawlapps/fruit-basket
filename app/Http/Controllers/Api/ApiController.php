<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\LineItem;
use App\Models\Order;
use App\Models\OrderColor;
use App\Models\User;
//use Illuminate\Http\Request;
use App\Traits\OrderController;
use Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Traits\PushNotificationTrait;

class ApiController extends Controller
{
    use PushNotificationTrait;
    use OrderController;
    public function login(Request $request){
        try{
            $rules = ['email' => 'required', 'password' => 'required'];

            $validator = Validator::make($request::all(), $rules);

            if($validator->fails())
                return response()->json(['error' => $validator->messages()], 422);

            $inputs = $request::all();

            $user = User::select('id', 'email', 'password', 'tags')->where('email', $inputs['email'])->first();
            $isSuccess =  false;
            if( $user ){
                if(Hash::check($inputs['password'], $user->password)) {
                    $isSuccess =  true;
                    $msg = 'Login successfully...';
                }else{
                    $msg = 'Incorrect password! please try again...';
                }
            }else{
                $msg = 'User not found...';
            }

            if( !$isSuccess ){
                return response()->json(['error' => ['message' => $msg ] ], 422);
            }else{
                return response()->json(['data' => $user], 200);
            }
        }catch( \Exception $e ){
            return response()->json(['error' => ['message' => $e->getMessage() ] ], 422);
        }
    }

    public function getOrders(Request $request){
        try{
            $rules = ['user_id' => 'required', 'source_name' => 'required', 'fulfillment_status' => 'required'];

            $validator = Validator::make($request::all(), $rules);

            if($validator->fails())
                return response()->json(['error' => $validator->messages()], 422);

            $inputs = $request::all();

            $user = User::find($inputs['user_id']);

            $msg = '';
            $isSuccess = false;
            if( $user ){
                $isSuccess =  true;
                $orders = $this->getApiOrderByUser($inputs, 'order');
            }else{
                $msg = 'Invalid user id...';
            }

            if( $isSuccess )
                return response()->json(['data' => $orders], 200);
            else
                return response()->json(['error' => ['message' => $msg ] ], 422);
        }catch( \Exception $e ){
            return response()->json(['error' => ['message' => $e->getMessage() ] ], 422);
        }
    }

    public function fulfilledLineItem(Request $request){
        try{
            $rules = ['user_id' => 'required', 'order_id' => 'required', 'lineitem_id' => 'required',];

            $validator = Validator::make($request::all(), $rules);

            if($validator->fails())
                return response()->json(['error' => $validator->messages()], 422);

            $inputs = $request::all();

            $user = User::find($inputs['user_id']);

            $msg = '';
            $isSuccess =  false;
            if( $user ){
                $isSuccess =  true;
                $result = $this->fulfilledOrder($inputs['user_id'], $inputs['order_id'], $inputs['lineitem_id']);
            }else{
                $msg = 'Invalid user id...';
            }

            if( $isSuccess )
                return response()->json(['data' => $result], 200);
            else
                return response()->json(['error' => ['message' => $msg ] ], 422);
        }catch( \Exception $e ){
            return response()->json(['error' => ['message' => $e->getMessage() ] ], 422);
        }
    }

    public function lineItemTotal(Request $request){
        try{
            $rules = ['user_id' => 'required', 'source_name' => 'required', 'fulfillment_status' => 'required'];

            $validator = Validator::make($request::all(), $rules);

            if($validator->fails())
                return response()->json(['error' => $validator->messages()], 422);

            $inputs = $request::all();

            $user = User::find($inputs['user_id']);

            $msg = '';
            $isSuccess =  false;
            if( $user ){
                $isSuccess =  true;
                $orders = $this->getApiOrderByUser($inputs, 'count');
            }else{
                $msg = 'Invalid user id...';
            }

            if( $isSuccess )
                return response()->json(['data' => $orders], 200);
            else
                return response()->json(['error' => ['message' => $msg ] ], 422);
        }catch( \Exception $e ){
            return response()->json(['error' => ['message' => $e->getMessage() ] ], 422);
        }
    }

    public function lineitemStatus(Request $request){
        try{
            $rules = ['order_id' => 'required', 'lineitem_id' => 'required', 'status' => 'required'];

            $validator = Validator::make($request::all(), $rules);

            if($validator->fails())
                return response()->json(['error' => $validator->messages()], 422);

            $inputs = $request::all();

            $lineitem = LineItem::where('lineitem_id', $inputs['lineitem_id'])->first();

            $msg = '';
            $isSuccess =  false;
            if( $lineitem ){
                $isSuccess =  true;
                $lineitem->status = $inputs['status'];
                $lineitem->save();

                $order = Order::where('order_id', $inputs['order_id'])->first();

                $tagged_status = $inputs['status'];
                if( $inputs['status'] != 'in_progress' ){
                    $IpLineItem = LineItem::where('db_order_id', $order->id)->where('status', 'in_progress')->where('lineitem_id', '!=', $inputs['lineitem_id'])->count();
                    $tagged_status = ($IpLineItem > 0) ? 'in_progress' : $inputs['status'];
                }
                $order->order_tagged = $tagged_status;
                $order->save();

                $msg = 'Status changed successfully...';
            }else{
                $msg = 'Invalid lineitem id...';
            }

            if( $isSuccess )
                return response()->json(['data' => $msg], 200);
            else
                return response()->json(['error' => ['message' => $msg ] ], 422);
        }catch( \Exception $e ){
            return response()->json(['error' => ['message' => $e->getMessage() ] ], 422);
        }
    }

    public function testNotification(){
        $data = [];
        // $data = [
        //     'key1' => 'value1',
        //     'key2' => 'value2'
        // ];
         $users = [2];
         $this->sendNotificationH($data, $users, 'Urgent order received!');
    }
}
