<?php

namespace App\Http\Controllers\Color;

use App\Http\Controllers\Controller;
use App\Models\OrderColor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ColorController extends Controller
{
    public function index(){
        try{
            $color = OrderColor::select('id', 'order_status', 'color')->get();

            $color = $color->map(function ($name) {
                return [
                    'id' => $name->id,
                    'order_status' => $name->order_status,
                    'color' => $name->color,
                    'displayColorPicker' => false,
                    'dispcolor' => [
                        'width'=>  '36px',
                        'height'=>  '100%',
                        'borderRadius'=>  '2px',
                        'background'=>  $name->color,
                    ],
                ];
            })->toArray();
            return response()->json(['data' => $color, 'isSuccess' => true], 200);
        }catch ( \Exception $e ){
            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }

    public function Store(Request $request){
        try{
            DB::beginTransaction();
            $data = $request->json()->all();

            foreach ( $data as $key=>$val ){
                $orderColor = OrderColor::find($val['id']);
                $orderColor->color = $val['color'];
                $orderColor->save();
            }

            DB::commit();
            $msg = 'Saved!';
            return response()->json(['data' => $msg, 'isSuccess' => true], 200);
        }catch ( \Exception $e ){
            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }
}
