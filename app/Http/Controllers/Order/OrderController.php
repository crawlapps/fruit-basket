<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use App\Models\LineItem;
use App\Models\Order;
use App\Models\OrderColor;
use App\Traits\PushNotificationTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    use PushNotificationTrait;
    use \App\Traits\OrderController;
    public function index(Request $request){
        try{
            $order = $this->getOrderByUser($request->user_id, $request->filter);

            return response()->json($order, 200);
        }catch ( \Exception $e ){
            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }

    public function Store(Request $request){
        try{
            DB::beginTransaction();
            $data = $request->json()->all();

            foreach ( $data as $key=>$val ){
                $order = Order::find($val['id']);
                if( $order ){
                    $old_order_is_urgent = $order->is_urgent;
                    $order->is_urgent = $val['is_urgent'];

                    $orderColor = OrderColor::get()->pluck('color', 'order_status')->toArray();
                    $IpLineItem = LineItem::where('db_order_id', $order->id)->where('status', 'in_progress')->count();
//                    $tagged_color = ($IpLineItem > 0) ? $orderColor['in_progress'] : $orderColor['urgent'];
                    $tagged_color = ($IpLineItem > 0) ? 'in_progress' : 'urgent';

                    $order->order_tagged = $tagged_color;
                    $order->save();

                    if( $val['is_urgent'] && !(bool)$old_order_is_urgent){
                        $users = $this->getTaggedUser($order->id);
                        $data = $this->getNotificationOrder($order->id);
                        $this->sendNotificationH($data, $users, 'Urgent order received!');
                    }
                }
            }

            DB::commit();
            $msg = 'Saved!';
            return response()->json(['data' => $msg, 'isSuccess' => true], 200);
        }catch ( \Exception $e ){
            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }

    public function orderList(Request $request){
        try{
            $filterDate = $request->filterDate;
            if( $filterDate == '' || $filterDate == null || $filterDate == 'null'){
                $entity = Order::all();
            }else{
                $entity = Order::where('delivery_date', $filterDate)->get();
            }
            $orders = $entity->map(function ($order) {
                return [
                    'id' => $order->id,
                    'name' => $order->name,
                    'total_price' => currencyH($order->currency).$order->price,
                    'fulfillment_status' => $order->fulfillment_status,
                    'delivery_date' => $order->delivery_date,
                    'active_modal' => false
                ];
            })->toArray();
            return response()->json(['data' => $orders, 'isSuccess' => true], 200);
        }catch ( \Exception $e ){
            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }

    public function reOrder(Request $request){
        try{
            DB::beginTransaction();
            $data = $request->json()->all();

            foreach ( $data as $key=>$val ){
                $order = Order::where('name', $val)->first();
                $order->position = $key;
                $order->save();
            }
            DB::commit();
            $msg = 'Saved!';
            return response()->json(['data' => $msg, 'isSuccess' => true], 200);
        }catch ( \Exception $e ){
            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }

    public function updateDeliveryDate(Request $request){

           logger("==updateDeliveryDate===");     

        try{
            DB::beginTransaction();
            $user = Auth::user();
            $data = $request->json()->all();


            $delivery_date = $data['date'];

            $db_order = Order::where('name', $data['name'])->first();


            if( $db_order ){
                $db_order->delivery_date = $delivery_date;
                $db_order->save();

                $sh_order_result = $user->api()->rest('GET', '/admin/api/2021-07/orders/' . $db_order->order_id . '.json');

                logger("----order-update-----");

                    logger("==delivery_type===");
                    logger($db_order->delivery_type);

                 $delivery_type = $db_order->delivery_type;
  
                if( !$sh_order_result['errors'] ) {
                    $sh_order = $sh_order_result['body']['order'];
                    $note_attr = $sh_order->note_attributes;

                    $attr = [];
                    $is_nttr = 0;
                    if( !empty($note_attr) ){


                            logger("===U::note_attr====");
                            logger(json_encode($note_attr));

                        foreach ( $note_attr as $nkey=>$nval ){
                          
                           if( $nval['name'] == 'Shipping-Date' ){
                                   $is_nttr = 1;                          
                                   $note_attr[$nkey]['value'] = $delivery_date;
 
                                 $dt['name'] = 'Shipping-Date';
                                 $dt['value'] = $delivery_date;

                                $attr[] = $dt;
                            }else  if( $nval['name'] == 'Delivery-Date' ){
                                   $is_nttr = 1;                          
                                   $note_attr[$nkey]['value'] = $delivery_date;
 
                                 $dt['name'] = 'Delivery-Date';
                                 $dt['value'] = $delivery_date;

                                $attr[] = $dt;
                            }else  if( $nval['name'] == 'Pickup-Date' ){
                                   $is_nttr = 1;                          
                                   $note_attr[$nkey]['value'] = $delivery_date;
 
                                 $dt['name'] = 'Pickup-Date';
                                 $dt['value'] = $delivery_date;

                                $attr[] = $dt;
                            }else{
                                $dt['name'] = $nval['name'];
                                $dt['value'] = $nval['value'];
                                $attr[] = $dt;
                            }
                        }
                    }

                    logger("================new note_attributes===========");
                    logger($attr);


                    if( $is_nttr == 0 ){
                        $attr = $note_attr->toArray();
                        $darr['name'] = 'delivery_date';
                        $darr['value'] = $delivery_date;
                        array_push($attr, $darr);
                    }

                    $parameter = [
                        'order' => [
                            'id' => $db_order->order_id,
                            'note_attributes' => $attr
                        ],
                    ];

                    $isSuccess = false;
                    $sh_order_result = $user->api()->rest('PUT', 'admin/orders/' . $db_order->order_id . '.json', $parameter);

                    if( !$sh_order_result['errors'] ){
                        DB::commit();
                        $isSuccess = true;
                        $msg = 'Saved!';
                    }else{
                        DB::rollBack();
                        $msg = 'Error!';
                    }
                }
                else{
                    return response()->json(['data' => 'Order not found!', 'isSuccess' => false], 422);
                }
            }

            return response()->json(['data' => $msg, 'isSuccess' => $isSuccess], 200);
        }catch ( \Exception $e ){
            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }
}
