<?php

namespace App\Http\Controllers\User;

use App\Events\CheckOrderUpdate;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\User;
use App\Traits\OrderController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    use OrderController;
    public function index( Request $request){
        try{
//            dd($this->getLocationId(1, '35934993907872'));
//            dd($this->getFulfillmentStatus(1, '3732416954528', 2));
//            event(new CheckOrderUpdate('3732416954528', 'crawlapps-info.myshopify.com'));
//            dd($this->fulfilledOrder(5, '3732416954528', '9667254419616'));

            $user = User::select('id', 'email', 'tags')->where('name', NULL)->get()->toArray();
            return response()->json(['data' => $user, 'isSuccess' => true], 200);
        }catch ( \Exception $e ){
            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }

    public function store(UserRequest $request){
        try{
            $data = $request->json()->all();

            DB::beginTransaction();

            $user = ( @$data['id'] ) ? User::find($data['id']) : new User;
            $user->email = $data['email'];
            $user->password = ($data['password'] == $user->password) ? $user->password : bcrypt($data['password']);
            $user->tags = str_replace(" ","",$data['tags']);
            $user->save();

            DB::commit();
            $msg = 'Saved!';
            return response()->json(['data' => $msg, 'isSuccess' => true], 200);
        }catch( \Exception $e ){
            DB::rollBack();
            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }

    public function edit(User $user){
        try{
            $res['id'] = $user->id;
            $res['email'] = $user->email;
            $res['password'] = $user->password;
            $res['tags'] = $user->tags;
            return response()->json(['data' => $res, 'isSuccess' => true], 200);
        }catch( \Exception $e ){
            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }

    public function destroy(User $user){
        try{
            $user->delete();
            return response()->json(['data' => 'Deleted', 'isSuccess' => true], 200);
        }catch( \Exception $e ){
            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }

    public function test( Request $request){
        $users = $this->getTaggedUser(2);
        $data = $this->getNotificationOrder(2);
//        OneSignal::sendNotificationUsingTags(
//            "New Order created!",
//            array(
//                ["field" => "tag", "key" => "user_id", "relation" => "=", "value" => '2'],
//            ),
//            $url = null,
//            $data = null,
//            $buttons = null,
//            $schedule = null
//        );
//        sendNotificationH($data, $users);
    }
}
