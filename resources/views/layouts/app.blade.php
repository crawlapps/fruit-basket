<html>
<head class="subscription-app">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Fruit Basket App') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- style custom css -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Polaris css cdn -->
    <link rel="stylesheet" href="https://unpkg.com/@shopify/polaris@6.0.1/dist/styles.css"/>

    {{--    font-awesome css cdn--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    @if(config('shopify-app.appbridge_enabled'))
        <script src="https://unpkg.com/@shopify/app-bridge"></script>
        <script>
            var AppBridge = window['app-bridge'];
            var createApp = AppBridge.default;
                window.shopify_app_bridge = {
                apiKey: '{{ config('shopify-app.api_key') }}',
                shopOrigin: '{{ Auth::user()->name }}',
                forceRedirect: true,
            };
        </script>
    @endif
</head>
<body class="">
<div id="app">
</div>
</body>
</html>
