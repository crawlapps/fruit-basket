import RoutePath from "../routes/index";
import {Link} from "react-router-dom";

import {Card, Tabs} from '@shopify/polaris';
import TokenIndex from '../../components/pages/User/index';
import React, {useCallback, useState, useEffect} from "react";
import { useHistory, useParams } from "react-router-dom";

function Menu() {
    const history = useHistory();
    const [selected, setSelected] = useState(0);

    const tabs = [
        {
            id: 'dashboard',
            content: 'Dashboard',
            accessibilityLabel: 'dashboard',
            panelID: 'dashboard',
            path: '/'
        },
        {
            id: 'users',
            content: 'Users',
            panelID: 'users',
            path: '/user-index/'
        },
        {
            id: 'color',
            content: 'Order Color',
            panelID: 'color',
            path: '/order-color/'
        },
        {
            id: 'orderlist',
            content: 'Order List',
            panelID: 'orderlist',
            path: '/order-list/'
        },
    ];
    const handleTabChange = useCallback(
        (selectedTabIndex) => setSelected(selectedTabIndex),
        [],

        history.push(tabs[selected].path)
    );

    return (
        <Card>
            <Tabs tabs={tabs} selected={selected} onSelect={handleTabChange}>
            </Tabs>
        </Card>
        // <div className="Polaris-Tabs__Wrapper">
        //     <ul role="tablist" className="Polaris-Tabs">
        //         <Link to='/'>
        //             <li role="presentation" className="Polaris-Tabs__TabContainer">
        //                 <a className="help__Tab Polaris-Tabs__Tab " target="_blank">
        //                     <span className="Polaris-Tabs__Title">Dashboard</span>
        //                 </a>
        //             </li>
        //         </Link>
        //         <Link to='/token-index'>
        //             <li role="presentation" className="Polaris-Tabs__TabContainer">
        //             <a className="help__Tab Polaris-Tabs__Tab " target="_blank">
        //                 <span className="Polaris-Tabs__Title">Tokens</span>
        //             </a>
        //             </li>
        //         </Link>
        //     </ul>
        // </div>
    );
}

export default Menu;
