import React from "react";
import {
    Switch,
    Route, BrowserRouter
} from "react-router-dom";

import Menu from "../layouts/menu";

import DashboardIndex from '../../components/pages/Dashboard/index';
import UserIndex from '../../components/pages/User/index';
import CreateUser from '../../components/pages/User/create';
// import OrderIndex from '../../components/pages/Orders/index';
import OrderColor from '../../components/pages/Color/index';
import OrderIndex from '../../components/pages/Orders/index-drag-drop';
import OrderList from '../../components/pages/Orders/list';
import Layouts from '../layouts';

export default function RoutePath(){
    return(
        <BrowserRouter>
            <Menu />
            <Switch>
                <Route exact path='/' component={DashboardIndex}/>
                <Route exact path='/user-index' component={UserIndex}/>
                <Route exact path='/create-user/:id' component={CreateUser}/>
                <Route exact path='/order-index/:id' component={OrderIndex}/>
                <Route exact path='/order-color' component={OrderColor}/>
                <Route exact path='/order-list' component={OrderList}/>
            </Switch>
        </BrowserRouter>
    )
}

