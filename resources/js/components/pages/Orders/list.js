import React, {useEffect, useState, useCallback} from "react";
import {Button, Card, Page, Icon, Modal, Stack} from "@shopify/polaris";
import {AddMajor, EditMinor} from '@shopify/polaris-icons';
import {Toast} from '@shopify/app-bridge-react';
// import Datetime from 'react-datetime';
// import "react-datetime/css/react-datetime.css";
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";
import Moment from 'react-moment';

function OrderList(){
    const [rows, setRows] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [showToast, setShowToast] = useState(false);
    const [errorToast, setErrorToast] = useState(false);
    const [active, setActive] = useState(false);
    const [updateDate, setUpdateDate] = useState([]);
    const [updateOrderName, setUpdateOrderName] = useState('');
    const [modalAction, setModalAction] = useState('add');
    const [filterDate, setFilterDate] = useState();

    function handleModalChange(index, action){
        if( typeof index != 'undefined'){
            if( rows[index]['delivery_date'] != '' ){
                setUpdateOrderName(rows[index]['name']);
                setModalAction('edit');
                let dt = rows[index]['delivery_date'];
                updateDate.delivery_date = new Date(dt);
            }else{
                setUpdateOrderName(rows[index]['name']);
                setModalAction('add');
                updateDate.delivery_date = new Date();
            }
            // setUpdateDate(rows[index]);
        }

        setActive(!active);
    }

    const handleClose = () => {
        handleModalChange();
    };

    const changeDeliveryDate = date => {

        let arr = {...updateDate};
        arr.delivery_date = date;
        setUpdateDate(arr);
    }

    const updateDeliveryDate = () => {
        let payload = {};
        payload.name = updateOrderName;
        payload.date = formatDate(updateDate.delivery_date);

        const response = fetch('/update-delivery-date', {
            method: 'POST', headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            body: JSON.stringify(payload)
        }).then(response => response.json())
            .then(res =>{
                if (res.isSuccess) {
                    setShowToast(true);
                    handleClose();
                    getData();
                } else {
                    console.log('ERROR:: ' + res.data);
                    setErrorToast(true);
                    handleClose();
                }
            })
    }
    function dismissToast() {
        setShowToast(false);
        setErrorToast(false);
    };
    useEffect(() => {
        getData();
    }, []);

    function formatDate(date){
        if (Object.prototype.toString.call(date) === "[object Date]") {
            // it is a date
            if (isNaN(date.getTime())) {  // d.valueOf() could also work
                return '';
            } else {
                let d = ("0" + date.getDate()).slice(-2);
                let m = ("0" + (date.getMonth() + 1)).slice(-2);
                let y = date.getFullYear();
                var s = date.getSeconds();
                var i = date.getMinutes();
                var h = date.getHours();
                // return y +'-'+m + '-'+d + ' ' + h + ':' + i + ':' + s;
                 return y +'-'+m + '-'+d;
            }
        } else {
            return '';
        }
    }

    async function getData(){;
        let filterdt = '';
        if( typeof filterDate != 'undefined'){
            filterdt = formatDate(filterDate);
        }
        const response = fetch('/order-list?filterDate=' + filterdt, {
            method: 'GET', headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

        }).then(response => response.json())
            .then(res =>{
                if (res.isSuccess) {
                    setRows(res.data);
                    setIsLoading(false);
                } else {
                    console.log('ERROR:: ' + res.data);
                }
            })
    }

    return (
        <div className="order-list-main">
            {showToast ? (
                <Toast content="Saved!" onDismiss={dismissToast} />
            ) : null}

            {errorToast ? (
                <Toast content="Order not found!" onDismiss={dismissToast} error/>
            ) : null}
            <Modal
                open={active}
                onClose={handleClose}
                title="Add/Edit Delivery Date"
                primaryAction={{
                    content: 'Save',
                    onAction: updateDeliveryDate,
                }}
                secondaryActions={[
                    {
                        content: 'Cancel',
                        onAction: handleClose,
                    },
                ]}
            >
                <Modal.Section>
                    <Stack vertical>
                        <Stack.Item>
                            <DatePicker
                                selected={
                                    ( modalAction == 'edit' ) ?
                                        Date.parse(updateDate.delivery_date)
                                    :
                                    updateDate.delivery_date
                                }
                                onChange={changeDeliveryDate}
                                timeInputLabel="Time:"
                                dateFormat="MM/dd/yyyy"
                                inline
                            />
                        </Stack.Item>
                    </Stack>
                </Modal.Section>
            </Modal>
            <Page title="Orders">
                <Card>
                    <div className='table_header_search'>
                        <DatePicker
                            selected={filterDate}
                            onChange={date => setFilterDate(date)}
                            timeInputLabel="Time:"
                            dateFormat="MM/dd/yyyy"
                        />
                        <Button onClick={getData}>Search</Button>
                    </div>

                    <div className='Polaris-DataTable'>
                        <div className='Polaris-DataTable__ScrollContainer'>
                            <table className='Polaris-DataTable__Table'>
                                <thead>
                                <th className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn Polaris-DataTable__Cell--header'><b>No</b></th>
                                <th className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn Polaris-DataTable__Cell--header'><b>Name</b></th>
                                <th className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn Polaris-DataTable__Cell--header'><b>Total</b></th>
                                <th className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn Polaris-DataTable__Cell--header'><b>Fulfillment</b></th>
                                <th className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn Polaris-DataTable__Cell--header'><b>Add/Edit Delivery Date</b></th>
                                </thead>

                                {(rows.length > 0 && !isLoading) ?
                                    <tbody>
                                    {rows.map((item, index) => {
                                        return (
                                            <tr key={index}>
                                                <td className='Polaris-DataTable__Cell'>{(index + 1)}.</td>
                                                <td className='Polaris-DataTable__Cell'>{item.name}</td>
                                                <td className='Polaris-DataTable__Cell'>{item.total_price}</td>
                                                <td className='Polaris-DataTable__Cell'>{item.fulfillment_status}</td>
                                                <td className='Polaris-DataTable__Cell delivery_date-button__icon'>
                                                    {item.delivery_date}                                                   
                                                </td>
                                            </tr>
                                        )
                                    })}
                                    </tbody>
                                    :
                                    <tbody>
                                    <tr>
                                        <td className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop text-center' colSpan='4'> {(isLoading) ? 'Loading...' : 'No data found...' } </td>
                                    </tr>
                                    </tbody>
                                }
                            </table>
                        </div>
                    </div>
                </Card>
            </Page>
        </div>
    );
}

export default OrderList;
