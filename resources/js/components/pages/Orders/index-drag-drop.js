import {Link, useParams} from "react-router-dom";
import {Toast} from '@shopify/app-bridge-react';
import React, {useEffect, useState, useCallback} from "react";
import {Button, Card, Page, ChoiceList, Filters, Checkbox} from "@shopify/polaris";
// import {SortableContainer, SortableElement, arrayMove} from 'react-sortable-hoc';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import {drop} from "react-beautiful-dnd/src/state/action-creators";

function OrderIndex(){
    const { id } = useParams();
    const [rows, setRows] = useState([]);
    const [saveAction, setSaveAction] = useState('urgent');
    const [isLoading, setIsLoading] = useState(true);
    const [showToast, setShowToast] = useState(false);
    const [queryValue, setQueryValue] = useState(null);
    const [fulfillmentStatus, setFulfillmentStatus] = useState(null);
    const [checked, setChecked] = useState(false);

    const filters = [
        {
            key: 'fulfillmentStatus',
            label: 'Fulfillment status',
            filter: (
                <ChoiceList
                    title="Fulfillment status"
                    titleHidden
                    choices={[
                        {label: 'Fulfilled', value: 'fulfilled'},
                        {label: 'Unfulfilled', value: 'unfulfilled'},
                    ]}
                    selected={fulfillmentStatus || []}
                    onChange={handleFulfillmentStatus}
                    allowMultiple
                />
            ),
            shortcut: true,
        }
    ];
    const appliedFilters = [];

    if (!isEmpty(fulfillmentStatus)) {
        const key = 'fulfillmentStatus';
        appliedFilters.push({
            key,
            label: disambiguateLabel(key, fulfillmentStatus),
            onRemove: handleFulfillmentStatusRemove,
        });
    }

    useEffect(() => {
        getData();
    }, []);

    async function getData(filter){
        filter = ( filter == '' || typeof filter == 'undefined') ? 'fulfilled,unfulfilled' : filter;
        const response = fetch('/order?user_id=' + id + '&filter=' + filter, {
            method: 'GET', headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

        }).then(response => response.json())
            .then(res =>{
                if (res.isSuccess) {
                    setRows(res.data);
                    setIsLoading(false);
                } else {
                    console.log('ERROR:: ' + res.data);
                }
            })
    }
    function disambiguateLabel(key, value) {
        switch (key) {
            case 'fulfillmentStatus':
                return value.map((val) => val).join(', ');
            default:
                return value;
        }
    }
    function isEmpty(value) {
        if (Array.isArray(value)) {
            return value.length === 0;
        } else {
            return value === '' || value == null;
        }
    }

    function dismissToast() {
        setShowToast(false);
    };

    const saveOrder = useCallback(async ()  => {
        const response = await fetch('/order', {
            method: 'POST', headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            body: JSON.stringify(rows)
        }).then( response => response.json()).then(res => {
            if (res.isSuccess) {
                setShowToast(true);
                getData('');
            } else {
                console.log('ERROR:: ' + res.data);
            }
        }).catch( err =>{
            console.log(err);
        });
    }, [rows, saveAction]);

    function handleChange(value, order){
        var arr = [...rows];
        arr[order].is_urgent = value;
        setRows(arr);
    }

    function handleFulfillmentStatus(value) {
        setFulfillmentStatus(value);
        getData(value);
    }

    function handleFulfillmentStatusRemove(){
        setFulfillmentStatus(null)
        getData('');
    }

    const handleFiltersClearAll = useCallback(() => {
        handleFulfillmentStatusRemove();
    }, [
        handleFulfillmentStatusRemove,
    ]);

    // original
    // function _renderObject(){
    //     // <SortableList
    //     //     items={rows}
    //     //     onSortEnd={onSortEnd}
    //     //     axis="xy"
    //     //     helperClass="SortableHelper"
    //     // />
    //     return Object.entries(rows).map(([pkey, pvalue], pi) => {
    //         return (
    //             // <Draggable key={pi} onDrag={handleDrag}>
    //             <div className="table_row" key={pi}>
    //                 <div className="table_head table_head_main">
    //                     <p className="order-name" style={{background: pvalue.order_tagged}}><b>{pkey}</b></p>
    //                     <div className="table_line_item">
    //                         <Checkbox
    //                             label="Is urgent"
    //                             checked={rows[pkey]['is_urgent']}
    //                             onChange={value => handleChange(value, pkey)}
    //                         />
    //                     </div>
    //                 </div>
    //                 <div className="table_head_title">
    //                     <div className="table_line_item"> <b>Line item</b></div>
    //                     <div className="table_status text-center"><b> Fulfillment status </b></div>
    //                     <div className="table_price"><b>Price</b></div>
    //                     <div className="table_status"><b>Status</b></div>
    //                     <div className="table_email"><b>Fulfilled By</b></div>
    //                     <div className="table_time"><b>Fulfilled At</b></div>
    //                 </div>
    //                 {Object.entries(pvalue).map(([ckey, cvalue], ci) => {
    //                     return (
    //                         <div className="table_body" key={ci}>
    //                             <div className="table_line_item"> {cvalue.title}</div>
    //                             <div className="table_status text-center"> {cvalue.fulfillment_status}</div>
    //                             <div className="table_price"><b>{cvalue.currency} {cvalue.price}</b></div>
    //                             <div className="table_status">{cvalue.status}</div>
    //                             <div className="table_email">{cvalue.fulfilled_by}</div>
    //                             <div className="table_time">{cvalue.fulfilled_at}</div>
    //                         </div>
    //                     )
    //                 })
    //                 }
    //             </div>
    //             // </Draggable>
    //         )
    //     })
    // }

    // working drag-drop
    // function _renderObject(){
    //     return (
    //         <DragDropContext onDragEnd={onDragEnd}>
    //             <Droppable droppableId="droppable">
    //                 {(provided, snapshot) => (
    //                     <div
    //                         {...provided.droppableProps}
    //                         ref={provided.innerRef}
    //                         style={getListStyle(snapshot.isDraggingOver)}
    //                     >
    //                         {items.map((item, index) => (
    //                             <Draggable key={index} draggableId={item.id} index={index}>
    //                                 {(provided, snapshot) => (
    //                                     <div
    //                                         ref={provided.innerRef}
    //                                         {...provided.draggableProps}
    //                                         {...provided.dragHandleProps}
    //                                         style={getItemStyle(
    //                                             snapshot.isDragging,
    //                                             provided.draggableProps.style
    //                                         )}
    //                                     >
    //                                         {item.content}
    //                                     </div>
    //                                 )}
    //                             </Draggable>
    //                         ))}
    //                         {provided.placeholder}
    //                     </div>
    //                 )}
    //             </Droppable>
    //         </DragDropContext>
    //     )
    // }

    // mt drag-drop
    function _renderObject(){
        return (
            <DragDropContext onDragEnd={onDragEnd}>
                <Droppable droppableId="droppable">
                    {(provided, snapshot) => (
                        // <div
                        //     {...provided.droppableProps}
                        //     ref={provided.innerRef}
                        //     style={getListStyle(snapshot.isDraggingOver)}
                        // >
                        <div
                            {...provided.droppableProps}
                            ref={provided.innerRef}
                        >
                            {rows.map((pvalue, pi) => {
                                return (
                                    <Draggable key={pvalue.order_name} draggableId={pvalue.order_name} index={pi}>
                                        {(provided, snapshot) => (
                                            // <div
                                            //     ref={provided.innerRef}
                                            //     {...provided.draggableProps}
                                            //     {...provided.dragHandleProps}
                                            //     style={getItemStyle(
                                            //         snapshot.isDragging,
                                            //         provided.draggableProps.style
                                            //     )}
                                            // >
                                            <div
                                                ref={provided.innerRef}
                                                {...provided.draggableProps}
                                                {...provided.dragHandleProps}
                                            >
                                                <div className="table_row" key={pi}>
                                                    <div className="table_head table_head_main">
                                                        <p className="order-name" style={{background: pvalue.order_tagged}}><b>{pvalue.order_name}</b></p>
                                                        <div className="table_line_item">
                                                            <Checkbox
                                                                label="Is urgent"
                                                                checked={rows[pi]['is_urgent']}
                                                                onChange={value => handleChange(value, pi)}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="order_notes">                                                 <p><b>Delivery Date:</b> { pvalue.delivery_date }</p>
                                                        <p><b>Note: </b> { pvalue.notes }</p>
                                                    </div>
                                                    <div className="table_head_title">
                                                        <div className="table_line_item"> <b>Line item</b></div>
                                                        <div className="table_status text-center"><b> Fulfillment status </b></div>
                                                        <div className="table_price"><b>Price</b></div>
                                                        <div className="table_status"><b>Status</b></div>
                                                        <div className="table_email"><b>Fulfilled By</b></div>
                                                        <div className="table_time"><b>Fulfilled At</b></div>
                                                    </div>
                                                    {pvalue['line_items'].map((cvalue, ci) => {
                                                        return (
                                                        <div className="table_body" key={ci}>
                                                        <div className="table_line_item"> {cvalue.title}</div>
                                                        <div className="table_status text-center"> {cvalue.fulfillment_status}</div>
                                                        <div className="table_price"><b>{cvalue.currency} {cvalue.price}</b></div>
                                                        <div className="table_status">{cvalue.status}</div>
                                                        <div className="table_email">{cvalue.fulfilled_by}</div>
                                                        <div className="table_time">{cvalue.fulfilled_at}</div>
                                                        </div>
                                                        )
                                                    })
                                                    }
                                                </div>
                                            </div>
                                        )}
                                    </Draggable>
                            )
                            })}
                            {provided.placeholder}
                        </div>
                    )}
                </Droppable>
            </DragDropContext>
        )
    }
    // dragdrop functions

    function onDragEnd(result) {
        // dropped outside the list
        if (!result.destination) {
            return;
        }
        const items = reorder(
            rows,
            result.source.index,
            result.destination.index
        );

        handleReOrder(items);
    }

    async function handleReOrder(items){
        setRows(items);

        let data = [];
        items.forEach(el => {
            data.push(el.order_name);
        });
        const response = await fetch('/reorder', {
            method: 'POST', headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            body: JSON.stringify(data)
        }).then( response => response.json()).then(res => {
            if (res.isSuccess) {
                getData('');
            } else {
                console.log('ERROR:: ' + res.data);
            }
        }).catch( err =>{
            console.log(err);
        });
    }
    function reorder (list, startIndex, endIndex){
        const result = Array.from(list);

        const [removed] = result.splice(startIndex, 1);

        result.splice(endIndex, 0, removed);
        return result;
    }

    return (
        <div className="order-index-main">
            {showToast ? (
                <Toast content="Saved!" onDismiss={dismissToast} />
            ) : null}
            <div className='text-left my-5 Polaris-Page'>
                <Link className='primary_black_color' to='/'><i className="fa fa-arrow-left mr-3" aria-hidden="true"></i>Back</Link>
            </div>
            <Page title="Orders">
                <div className='text-right my-4'>
                    <Button primary onClick={saveOrder}>Save</Button>
                </div>
                <Card sectioned>
                    <div className='Polaris-DataTable'>
                        <Filters
                            queryValue={queryValue}
                            filters={filters}
                            appliedFilters={appliedFilters}
                            onClearAll={handleFiltersClearAll}
                        />
                        <div className='Polaris-DataTable__ScrollContainer'>
                            {(rows.length > 0 && !isLoading) ?
                                <div>
                                    {_renderObject()}

                                </div>
                                :
                                <div>
                                    <p className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop     text-center' colSpan='4'> {(isLoading) ? 'Loading...' : 'No data found...' } </p>
                                </div>
                            }
                        </div>
                    </div>
                </Card>
            </Page>
        </div>
    );
}

export default OrderIndex;
