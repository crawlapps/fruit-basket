import React, {useEffect, useState, useCallback} from "react";
import reactCSS from 'reactcss'
import { ChromePicker } from 'react-color';
import {Button, Card, Filters, Page, Heading, TextField} from "@shopify/polaris";
import {Toast} from "@shopify/app-bridge-react";

function OrderColor(){
    const [rows, setRows] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [displayColorPicker, setDisplayColorPicker] = useState(true);
    const [showToast, setShowToast] = useState(false);
    const styles = reactCSS({
        'default': {
            color: {
                width: '36px',
                height: '14px',
                borderRadius: '2px',
                // background: `rgba(${ this.state.color.r }, ${ this.state.color.g }, ${ this.state.color.b }, ${ this.state.color.a })`,
                background: `rgba(0,0,0,.1)`,
            },
            swatch: {
                background: '#fff',
                borderRadius: '1px',
                boxShadow: '0 0 0 1px rgba(0,0,0,.1)',
                display: 'inline-block',
                cursor: 'pointer',
            },
            popover: {
                position: 'absolute',
                zIndex: '2',
            },
            cover: {
                position: 'fixed',
                top: '0px',
                right: '0px',
                bottom: '0px',
                left: '0px',
            },
        },
    });

    useEffect(() => {
        getData();
    }, []);

    async function getData(){
        const response = fetch('/color', {
            method: 'GET', headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

        }).then(response => response.json())
            .then(res =>{
                if (res.isSuccess) {
                    setRows(res.data);
                    setIsLoading(false);
                } else {
                    console.log('ERROR:: ' + res.data);
                }
            })
    }

    const saveColor = useCallback(async ()  => {
        const response = await fetch('/color', {
            method: 'POST', headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            body: JSON.stringify(rows)
        }).then( response => response.json()).then(res => {
            if (res.isSuccess) {
                setShowToast(true);
                getData('');
            } else {
                console.log('ERROR:: ' + res.data);
            }
        }).catch( err =>{
            console.log(err);
        });
    }, [rows]);

    function handleChangeComplete(color, index){
        var arr = [...rows];
        arr[index].color = color['hex'];

        var obj = {...rows[index]['dispcolor']};
        obj.background = color['hex'];
        arr[index]['dispcolor'] = obj;

        setRows(arr);
    }

    function handleClose(index) {
        var arr = [...rows];
        arr[index].displayColorPicker = false;
        setRows(arr);
    };

    function handleClick(index){
        var arr = [...rows];
        arr[index].displayColorPicker = !arr[index].displayColorPicker;
        setRows(arr);
    };

    function dismissToast() {
        setShowToast(false);
    };

    return (
        <div className="order-color-main">
            {showToast ? (
                <Toast content="Saved!" onDismiss={dismissToast} />
            ) : null}
            <Page title="Colors">
                <div className='text-right my-4'>
                    <Button primary onClick={saveColor}>Save</Button>
                </div>
                <Card sectioned>
                    <div className='Polaris-DataTable'>
                        {(rows.length > 0 && !isLoading) ?
                            <ul>
                                {rows.map((item, index) => {
                                    return (
                                        <li key={index}>
                                            <div className="order-status-listing">
                                                <Heading>Order Status :: </Heading>
                                                <p className="text-capitalize"> &nbsp; {item.order_status}</p>
                                            </div>
                                            <div className="order-color-input">
                                                <TextField label="" name="color" value={rows[index].color}  placeholder="" />
                                                <div style={ styles.swatch } onClick={value => handleClick(index)}>
                                                    <div style={ rows[index].dispcolor } />
                                                </div>
                                                { rows[index].displayColorPicker ? <div className="custom-chrome-picker" style={ styles.popover }>
                                                    <div style={ styles.cover } onClick={value => handleClose(index)}/>
                                                    <ChromePicker color={ rows[index].color } onChange={value => handleChangeComplete(value, index)} />
                                                </div> : null }
                                            </div>
                                            {/*<Heading>Order Status ::</Heading><p className="text-capitalize">{item.order_status}</p>*/}
                                            {/*<div>*/}
                                            {/*    <ChromePicker*/}
                                            {/*        color={ rows[index].color }*/}
                                            {/*        onChangeComplete={value => handleChangeComplete(value, index)}*/}
                                            {/*    />*/}
                                            {/*</div>*/}
                                        </li>
                                    )
                                })
                                }
                            </ul>
                            :
                            <div>
                                <p className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop     text-center' colSpan='4'> {(isLoading) ? 'Loading...' : 'No data found...' } </p>
                            </div>
                        }

                    </div>
                </Card>
            </Page>
        </div>
    );
}

export default OrderColor;
