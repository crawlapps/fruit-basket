import {useEffect, useState} from "react";
import {Modal} from "@shopify/app-bridge-react";
import {Button, Card, Page} from "@shopify/polaris";
import {Link} from "react-router-dom";

function DashboardIndex() {
    const [isLoading, setIsLoading] = useState(true);
    const [rows, setRows] = useState([]);

    useEffect(() => {
        getData();
    }, []);

    async function getData(){
        const response = fetch('/user', {
            method: 'GET', headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        }).then(response => response.json())
            .then(res =>{
                if (res.isSuccess) {
                    setRows(res.data);
                    setIsLoading(false);
                } else {
                    console.log('ERROR:: ' + res.data);
                }
            })
    }

    return (
        <div className="dashboard-index-main">
            <Page title="Users">
                <Card>
                    <div className='Polaris-DataTable'>
                        <div className='Polaris-DataTable__ScrollContainer'>
                            <table className='Polaris-DataTable__Table'>
                                <thead>
                                <th className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn Polaris-DataTable__Cell--header'>No</th>
                                <th className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn Polaris-DataTable__Cell--header'>Email</th>
                                </thead>

                                {(rows.length > 0 && !isLoading) ?
                                    <tbody>
                                    {rows.map((item, index) => {
                                        return (
                                            <tr key={index}>
                                                <td className='Polaris-DataTable__Cell'>{(index + 1)}.</td>
                                                <td className='Polaris-DataTable__Cell'><Link to={`/order-index/${item.id}`}>{item.email}</Link></td>
                                            </tr>
                                        )
                                    })}
                                    </tbody>
                                    :
                                    <tbody>
                                    <tr>
                                        <td className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop text-center' colSpan='2'> {(isLoading) ? 'Loading...' : 'No data found...' } </td>
                                    </tr>
                                    </tbody>
                                }
                            </table>
                        </div>
                    </div>
                </Card>
            </Page>
        </div>
    )
}

export default DashboardIndex;
