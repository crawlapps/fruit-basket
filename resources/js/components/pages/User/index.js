import {Link, useHistory} from "react-router-dom";
import {Provider, Modal} from '@shopify/app-bridge-react';
import {useState, useEffect} from "react";
import {Page, Card, DataTable, Button} from '@shopify/polaris';
import RoutePath from '../../routes/index';

function UserIndex(){
    const history = useHistory();
    const [rows, setRows] = useState([]);
    const [showModal, setShowModal] = useState(false);
    const [deleteUser, setDeleteUser] = useState({id: '', name: ''});
    const [primaryContent, setPrimaryContent] = useState('');
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        getData();
    }, []);

    async function getData(){
        const response = fetch('/user', {
            method: 'GET', headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        }).then(response => response.json())
            .then(res =>{
                if (res.isSuccess) {
                    setRows(res.data);
                    setIsLoading(false);
                } else {
                    console.log('ERROR:: ' + res.data);
                }
            })
    }

    function editToken(id){
        history.push('/create-user/' + id)
    }

    async function confirmRemoveUser(id, name){
        setDeleteUser({id: id, name: name});
        setPrimaryContent("Are you sure to delete '"+ name +"' user?");
        setShowModal(true);
    }

    async function removeUser(){
        let id = deleteUser.id;
        const response = await fetch('/user/' + id, {
            method: 'DELETE', headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        }).then( response => response.json()).then(res => {
            if (res.isSuccess) {
                setShowModal(false);
                getData();
            } else {
                console.log('ERROR:: ' + res.data);
            }
        });
    }

    function onActionCallback(value){
        console.log(value);
    }

    function getOrder(id){
        history.push('/order-index/' + id)
    }

    function onCloseCallback(value){
        setShowModal( false)
    }
    return (
        <div className="user-index-main">
            <Modal
                title="Remove User!"
                message={primaryContent}
                open={showModal}
                primaryAction={{
                    content: 'OK',
                    onAction: removeUser,
                }}
                secondaryActions={[
                    {
                        content: 'Cancel',
                        onAction: onCloseCallback,
                    },
                ]}
                onClose={onCloseCallback}
                onAction={value => onActionCallback(value)}
            />
            <Page title="Users">
                <div className='text-right my-4'>
                    <Link to='/create-user/0'><Button primary>Create User</Button></Link>
                </div>
                <Card>
                    <div className='Polaris-DataTable'>
                        <div className='Polaris-DataTable__ScrollContainer'>
                            <table className='Polaris-DataTable__Table'>
                                <thead>
                                <th className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn Polaris-DataTable__Cell--header'>No</th>
                                <th className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn Polaris-DataTable__Cell--header'>Email</th>
                                <th className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn Polaris-DataTable__Cell--header'>Tags</th>
                                <th className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn Polaris-DataTable__Cell--header'>Action</th>
                                </thead>

                                {(rows.length > 0 && !isLoading) ?
                                    <tbody>
                                    {rows.map((item, index) => {
                                        return (
                                            <tr key={index}>
                                                <td className='Polaris-DataTable__Cell'>{(index + 1)}.</td>
                                                <td className='Polaris-DataTable__Cell'>{item.email}</td>
                                                <td className='Polaris-DataTable__Cell'>{item.tags}</td>
                                                <td className='Polaris-DataTable__Cell'>
                                                    <Button className='fruit-basket_primary_btn' onClick={() => editToken(item.id)}>
                                                        <i className="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                    </Button>
                                                    <Button className='fruit-basket_primary_btn' onClick={() => confirmRemoveUser(item.id, item.email)}>
                                                        <i className="fa fa-trash-o" aria-hidden="true"></i>
                                                    </Button>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                    </tbody>
                                    :
                                    <tbody>
                                    <tr>
                                        <td className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop text-center' colSpan='4'> {(isLoading) ? 'Loading...' : 'No data found...' } </td>
                                    </tr>
                                    </tbody>
                                }
                            </table>
                        </div>
                    </div>
                </Card>
            </Page>
        </div>
    )
}
export default UserIndex;
