import React, {useCallback, useEffect, useState} from 'react';
import {TextField, Button, InlineError} from '@shopify/polaris';
import {Toast} from '@shopify/app-bridge-react';
import RoutePath from "../../routes";
import { useHistory, useParams } from "react-router-dom";
import {Link} from "react-router-dom";

function CreateUser(){
    const { id } = useParams();
    const [user, setUser] = useState({id: '', email: '', password: '', tags: ''});
    const [ readOnly ] = useState((id) ? true : false);

    const [errors, setErrors] = useState([]);
    const history = useHistory();

    function handleChange(value, action){
        if( action == 'email' ){
            setUser({id: user.id, email: value, password: user.password, tags: user.tags});
        }else if( action == 'password' ){
            setUser({id: user.id, email: user.email, password: value, tags: user.tags});
        }else if( action == 'tags' ){
            setUser({id: user.id, email: user.email, password: user.password, tags: value});
        }
    }

    useEffect(() => {
        // get data from serve of current plan to edit
        (async () => {
            if( id != 0 ){
                const response = await fetch('/user/' + id + '/edit', {
                    method: 'GET', headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                }).then(response => response.json())
                    .then(res =>{
                        if (res.isSuccess) {
                            setUser(res.data);
                        } else {
                            console.log('ERROR:: ' + res.data);
                        }
                    })
            }
        })();
    }, []);

    const saveToken = useCallback(async ()  => {
        const response = await fetch('/user', {
            method: 'POST', headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            body: JSON.stringify(user)
        }).then( response => response.json()).then(res => {
            if (res.isSuccess) {
                history.push('/user-index');
            } else {
                console.log('ERROR:: ' + res.data);
                setErrors(res);
            }
        }).catch( err =>{
            console.log(err);
            setErrors(err);
        });
    }, [user]);

    return (
        <div className="create-user-main Polaris-Page">
            <div className='text-left my-5'>
                <Link className='primary_black_color' to='/user-index/'><i className="fa fa-arrow-left mr-3" aria-hidden="true"></i>Back</Link>
            </div>
            <div className="Polaris-Card">
                <div className="Polaris-Card__Section pb-5">
                    <div className="create-user">
                        <TextField label="Email" name="email" value={user.email} onChange={value => handleChange(value, 'email')} placeholder="" />
                        {(errors['email']) ?
                            <InlineError message={errors['email']} fieldID="myFieldID" />
                            : null
                        }
                        <TextField label="Password" type="password" name="password" value={user.password} onChange={value => handleChange(value, 'password')} placeholder=""/>
                        {(errors['password']) ?
                            <InlineError message="required" fieldID="myFieldID" />
                            : null
                        }
                        <TextField label="Tag" name="tags" value={user.tags} onChange={value => handleChange(value, 'tags')} placeholder="" />
                        {(errors['tags']) ?
                            <InlineError message="required" fieldID="myFieldID" />
                            : null
                        }
                    </div>
                </div>
            </div>
            <div className='text-right mt-3 pt-4'>
                <Button onClick={saveToken} primary>Save</Button>
            </div>
        </div>
    );
}

export default CreateUser;
