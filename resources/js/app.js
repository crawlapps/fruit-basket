import RoutePath from "./components/routes";

require('./bootstrap');
import React, { Component, useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import {AppProvider} from "@shopify/polaris";
import en from '@shopify/polaris/locales/en.json';
import Index from "./components/layouts";
import { Provider } from '@shopify/app-bridge-react';
const config = window.shopify_app_bridge;

class App extends Component {
    render() {
        return (
            <div className="App">
                <RoutePath />
            </div>
        );
    }
}
if (document.getElementById('app')) {
    ReactDOM.render(
        <Provider
            config={config}
        >
            <AppProvider i18n={en} theme={{colorScheme: "light"}}>
                <App />
            </AppProvider>
        </Provider>,
        document.getElementById("app"));
}
