<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLineItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('line_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('db_order_id')->nullable()->comment('ID from orders table');
            $table->string('lineitem_id')->nullable();
            $table->string('order_id')->nullable();
            $table->string('product_id')->nullable();
            $table->string('variant_id')->nullable();
            $table->string('location_id')->nullable();
            $table->string('title')->nullable();
            $table->longText('image')->nullable();
            $table->integer('qty')->nullable();
            $table->string('sku')->nullable();
            $table->double('price')->nullable();
            $table->string('weight')->nullable();
            $table->string('weight_unit')->nullable();
            $table->string('fulfillment_status')->nullable();
            $table->unsignedBigInteger('fulfilled_by')->nullable()->comment('user id from users table');
            $table->dateTime('fulfilled_at')->nullable();
            $table->string('tag')->nullable();

            $table->timestamps();

            $table->foreign('db_order_id')->on('orders')->references('id')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('line_items');
    }
}
