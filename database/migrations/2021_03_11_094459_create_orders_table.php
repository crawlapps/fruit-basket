<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('order_id')->nullable();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('currency')->nullable();
            $table->double('price')->nullable();
            $table->string('financial_status')->nullable();
            $table->string('fulfillment_status')->nullable();
            $table->string('source_name')->nullable();
            $table->dateTime('order_created_at')->nullable();
            $table->dateTime('cancelled_at')->nullable();
            $table->integer('position')->nullable()->default(0);
            $table->boolean('is_urgent')->nullable()->default(0);
            $table->string('order_tagged')->nullable()->comment('red, green, blue...');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
